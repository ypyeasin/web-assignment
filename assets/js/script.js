$(function(){
    
    //  Basic configurations
    var API_URL = "http://54.213.40.201:8080/";

    //  Helper functions
    function isValidEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    //  Send data to server on initial load
    (function(){        
        var api = API_URL + "Track/1/anon";
        $.ajax({
            url: api,            
            type: "POST",
            data : {
                test: "some data"
            }                
        });        
        console.log("Data sent");
    })();

    function sendToClearbit(){
        $('#well').addClass('hidden');
        var input = $(this).val();
        
        if( !isValidEmail(input) ) return;

        var api = API_URL + "Track/1/person";
        $.ajax({
            url: api,            
            type: "POST",
            data : { email : input },
            success: function(data){                
                $('#well').removeClass('hidden');
                $('#name').html(data.name.fullName);
                $('#company').html(data.company);
                $('#city').html(data.geo.city);
                $('#gender').html(data.gender);                
            },
            error: function(a,b){
                console.log(a,b);
                alert('No data found');
            }
        });        
    }

    //  Checking clearbit information on valid email address
    $('#emaiAddress').keyup(sendToClearbit);
    $('#emaiAddress').blur(sendToClearbit);

    //  Learn more btn click event
    $('#learnMore').click(function(){
        var input = $("#emaiAddress").val();
        if( !isValidEmail(input) ) return;

        var api = API_URL + "Track/1/lead";
        $.ajax({
            url: api,
            type: "POST",
            data : { email : input },
            success: function(data){
                if( data[0].isLead )
                    alert("Marked as Lead");
            },
            error: function(a,b){
                console.log(a,b);
            }
        });        
    })
    
})