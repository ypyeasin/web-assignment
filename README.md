# Developer Coding Assignment
### By Md. Yeasin Hossain

# Prerequisits
  - node.js
  - npm
  

# Project setup
Issue following commands to instal the dependencies:
```sh
$ npm install http-server -g
```

# Running the application
Issue the following command to bootup the server:
** Make sure that service-assignment is running on port 1337
```sh
$ npm start
```